#include <stdio.h>
#include <stdlib.h>


long long fatorial(long long n)
{
	int i;
	long long fat = 1;
	for (i = 1; i <= n; i++)
		fat = fat * i;
	return fat;
}

int main(int argc, char **argv, char **envp)
{
	if(argc >= 1)
	{
		printf("%lld\n", fatorial(atoll(argv[1])));
	}
	else
	{
		puts("Voce precisa dar um valor para n!");
	}
}